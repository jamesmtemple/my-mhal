<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');

            $table->text('first_name');
            $table->text('last_name');
            $table->text('middle_initial')->nullable();

            $table->text('address_line_one');
            $table->text('address_line_two')->nullable();
            $table->text('city');
            $table->text('state');
            $table->text('zip');

            $table->text('phone_number')->nullable();

            $table->integer('craft');
            $table->integer('status');
            $table->string('level')->nullable();
            $table->string('step')->nullable();

            $table->string('duty_hours')->nullable();
            $table->string('off_days')->nullable();
            $table->boolean('preference_eligible')->default(false);
            $table->boolean('is_member');

            $table->string('finance_number');
            $table->string('ein');

            $table->timestamp('seniority_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
