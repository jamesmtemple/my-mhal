<?php namespace App\Notifications\Accounts;
    use Illuminate\Bus\Queueable;
    use Illuminate\Notifications\Notification;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Notifications\Messages\MailMessage;

    class AccountWasCreated extends Notification
    {
        use Queueable;

        private $password;

        /**
         * Create a new notification instance.
         *
         * @return void
         */
        public function __construct($password)
        {
            $this->password = $password;
        }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function via($notifiable)
        {
            return ['mail'];
        }

        /**
         * Get the mail representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
        {
            return (new MailMessage)
              ->subject("Welcome to the Mid-Hudson Area Local!")
              ->greeting("Hey {$notifiable->profile->first_name}!")
              ->line("An Administrator has created an account for you on the MyMHAL Portal. You can find your credentials below:")
              ->line("Username: {$notifiable->username}")
              ->line("Password: {$this->password}")
              ->action("Access MyMHAL", route("login"));
        }

        /**
         * Get the array representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function toArray($notifiable)
        {
            return [
                //
            ];
        }
    }
