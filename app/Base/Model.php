<?php namespace App\Base;
    use Illuminate\Contracts\Encryption\DecryptException;

    class Model extends \Illuminate\Database\Eloquent\Model
    {
        protected $encrypts = [];
        protected $hashes   = [];

        public function setAttribute($key, $value)
        {
            if(in_array($key, $this->hashes)){
                parent::setAttribute("_" . $key, hash('sha256',$value));
            }

            if(in_array($key, $this->encrypts)){
                return parent::setAttribute($key, encrypt($value));
            }

            return parent::setAttribute($key, $value);
        }

        public function getAttribute($key)
        {
            if(in_array($key, $this->encrypts)){
                if(isset($this->attributes[$key]) && $this->attributes[$key] != null && $this->attributes[$key] != "null"){
                    return decrypt(parent::getAttribute($key));
                }
            }

            return parent::getAttribute($key);
        }

        public function toArray()
        {
            $attributes = parent::toArray();

            foreach($attributes as $attribute => $value){
                if(in_array($attribute, $this->encrypts)){
                    try {
                        $attributes[$attribute]  = decrypt($value);
                    } catch (DecryptException $e) {

                    }
                }
            }

            return $attributes;
        }

    }
