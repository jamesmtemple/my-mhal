<?php namespace App\Models;
    use App\Base\Model;

    class Unit extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name',
            'address_line_one',
            'address_line_two',
            'city',
            'state',
            'zip',
            'phone_number',
            'fax_number',
            'finance_number',
            'level'
        ];

        /**
         * The attributes that are encrypted.
         *
         * @var array
         */
        protected $encrypts = [
          'name',
          'address_line_one',
          'address_line_two',
          'city',
          'state',
          'zip',
          'phone_number',
          'fax_number'
        ];
    }
