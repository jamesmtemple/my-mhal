<?php namespace App\Models;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Auth\Authenticatable;
    use Illuminate\Foundation\Auth\Access\Authorizable;
    use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
    use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
    use App\Base\Model;

    class User extends Model implements AuthenticatableContract, AuthorizableContract
    {
        use Notifiable, Authorizable, Authenticatable;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'ein',
            'username',
            'email',
            'password',
            'type',
            'authorized_payee',
            'authorized_signer'
        ];

        /**
         * The attributes that are encrypted.
         *
         * @var array
         */
        protected $encrypts = [
          'username',
          'email'
        ];

        /**
         * The attributes that are hashed.
         *
         * @var array
         */
        protected $hashes = [
          'username',
          'email'
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password',
            'remember_token',
        ];

        /**
         * The attributes that appended to the nodel.
         *
         * @var array
         */
         protected $appends = [
           'type_text',
           'is_admin',
           'is_board'
         ];

         /**
          * The attributes that are casted to a different type.
          *
          * @var array
          */
          protected $casts = [
            'authorized_payee'        => 'boolean',
            'authorized_signer'       => 'boolean'
          ];

         public function getIsAdminAttribute(){
           return $this->type >= 11;
         }

         public function getIsBoardAttribute(){
           return $this->type >= 2;
         }

         public function getTypeTextAttribute(){
           switch($this->type){
              case 0:
                return "Member";
              case 1:
                return "Steward";
              case 2:
                return "Director, Research and Education";
              case 3:
                return "Director, Human Relations";
              case 4:
                return "Director, Organization";
              case 5:
                return "Segeant at Arms";
              case 6:
                return "Chief Steward";
              case 7:
                return "Director, Clerk Craft";
              case 8:
                return "Director, Maintenance Craft";
              case 9:
                return "Recording Secretary";
              case 10:
                return "Corresponding Secretary";
              case 11:
                return "Trustee";
              case 12:
                return "Treasurer";
              case 13:
                return "Vice President";
              case 14:
                return "President";

           }
         }

         public function profile(){
             return $this->hasOne(Employee::class, "ein", "ein");
         }
    }
