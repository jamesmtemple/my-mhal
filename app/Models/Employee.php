<?php namespace App\Models;
    use App\Base\Model;

    class Employee extends Model
    {
          /**
           * The attributes that are mass assignable.
           *
           * @var array
           */
          protected $fillable = [
              'first_name',
              'last_name',
              'middle_initial',
              'address_line_one',
              'address_line_two',
              'city',
              'state',
              'zip',
              'phone_number',
              'finance_number',
              'ein',
              'level',
              'step',
              'off_days',
              'duty_hours',
              'preference_eligible',
              'craft',
              'status',
              'is_member',
              'seniority_date'
          ];

          /**
           * The attributes that are encrypted.
           *
           * @var array
           */
          protected $encrypts = [
            'first_name',
            'middle_initial',
            'last_name',
            'address_line_one',
            'address_line_two',
            'city',
            'state',
            'zip',
            'phone_number',
            'off_days',
            'duty_hours'
          ];

          protected $dates = [
            'seniority_date'
          ];
    }
