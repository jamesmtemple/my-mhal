<?php namespace App\Http\Controllers\System;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Models\User;
    use Illuminate\Support\Facades\Hash;
    use App\Notifications\Accounts\AccountWasCreated;
    use Illuminate\Support\Facades\Auth;

    class UsersController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $collection = User::paginate(25);

            return view('system.users.index')
              ->withUsers($collection);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('system.users.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->validate([
              'ein'           => ['required', 'unique:users', 'min:8', 'max:8'],
              'username'      => ['required', 'min:5','max:30'],
              'email'         => ['required', 'string','email','max:255'],
            ]);

            $password = str_random(25);

            $user = User::create([
                'ein'               => $request->ein,
                'username'          => $request->username,
                'email'             => $request->email,
                'password'          => Hash::make($password),
                'type'              => $request->type,
                'authorized_payee'  => $request->has('authorized_payee'),
                'authorized_signer' => $request->has('authorized_signer')
            ]);

            $user->notify(new AccountWasCreated($password));

            return redirect()->route('users.index')
              ->withStatus("The user account was created successfully!");
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(User $user)
        {
            return view('system.users.edit')
              ->withUser($user);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, User $user)
        {
            $attributes = [
                'username'          => $request->username,
                'email'             => $request->email,
                'type'              => $request->type
            ];

            if(Auth::user()->type === 12){
              $attributes['authorized_payee'] = $request->has('authorized_payee');
              $attributes['authorized_signer'] = $request->has('authorized_signer');
            }
            $user->update($attributes);

            return redirect()->route('users.index')
              ->withStatus("The user account was editted successfully!");
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(User $user)
        {
            $user->delete();

            return redirect()->route('users.index')
              ->withStatus("The user account was deleted successfully!");
        }
    }
