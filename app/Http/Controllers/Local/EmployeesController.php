<?php namespace App\Http\Controllers\Local;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Models\Employee;
    use Carbon\Carbon;


    class EmployeesController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $collection = Employee::paginate(25);

            return view('local.employees.index')
              ->withEmployees($collection);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('local.employees.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
              $request->validate([
                  'first_name'            => ['required'],
                  'last_name'             => ['required'],
                  'address_line_one'      => ['required'],
                  'city'                  => ['required'],
                  'state'                 => ['required'],
                  'zip'                   => ['required'],
                  'finance_number'        => ['required'],
                  'ein'                   => ['required'],
              ]);

              Employee::create([
                'first_name'              => $request->first_name,
                'middle_initial'          => $request->middle_initial,
                'last_name'               => $request->last_name,
                'ein'                     => $request->ein,
                'finance_number'          => $request->finance_number,
                'craft'                   => $request->craft,
                'status'                  => $request->status,
                'level'                   => $request->level,
                'step'                    => $request->step,
                'address_line_one'        => $request->address_line_one,
                'address_line_two'        => $request->address_line_two,
                'city'                    => $request->city,
                'state'                   => $request->state,
                'zip'                     => $request->zip,
                'level'                   => $request->level,
                'finance_number'          => $request->finance_number,
                'phone_number'            => $request->phone_number,
                'duty_hours'              => $request->duty_hours,
                'off_days'                => $request->duty_hours,
                'seniority_date'          => Carbon::parse($request->seniority_date),
                'preference_eligible'     => $request->has('preference_eligible'),
                'is_member'               => $request->has('is_member')
              ]);

              return redirect()->route('employees.index')
                ->withStatus("The employee was created successfully!");
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($ein)
        {
            $employee = Employee::where('ein',$ein)
              ->first();

            if(is_null($employee)){
                abort(404, "No employee was found");
            }

            return [
              'first_name'          => $employee->first_name,
              'middle_initial'      => $employee->middle_initial,
              'last_name'           => $employee->last_name,
              'finance_number'      => $employee->finance_number,
              'status'              => $employee->status
            ];
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(Employee $employee)
        {
            return view('local.employees.edit')
              ->withEmployee($employee);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Employee $employee)
        {
            $request->validate([
                'first_name'            => ['required'],
                'last_name'             => ['required'],
                'address_line_one'      => ['required'],
                'city'                  => ['required'],
                'state'                 => ['required'],
                'zip'                   => ['required'],
                'finance_number'        => ['required'],
                'ein'                   => ['required']
            ]);

            $employee->update([
              'first_name'              => $request->first_name,
              'middle_initial'          => $request->middle_initial,
              'last_name'               => $request->last_name,
              'ein'                     => $request->ein,
              'finance_number'          => $request->finance_number,
              'craft'                   => $request->craft,
              'status'                  => $request->status,
              'level'                   => $request->level,
              'step'                    => $request->step,
              'address_line_one'        => $request->address_line_one,
              'address_line_two'        => $request->address_line_two,
              'city'                    => $request->city,
              'state'                   => $request->state,
              'zip'                     => $request->zip,
              'level'                   => $request->level,
              'finance_number'          => $request->finance_number,
              'phone_number'            => $request->phone_number,
              'duty_hours'              => $request->duty_hours,
              'off_days'                => $request->duty_hours,
              'seniority_date'          => Carbon::parse($request->seniority_date),
              'preference_eligible'     => $request->has('preference_eligible'),
              'is_member'               => $request->has('is_member')
            ]);

            return redirect()->route('employees.index')
              ->withStatus('The employee was updated successfully!');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(Employee $employee)
        {
            $employee->delete();

            return redirect()->route('employees.index')
              ->withStatus('The employee has been deleted successfully!');
        }
    }
