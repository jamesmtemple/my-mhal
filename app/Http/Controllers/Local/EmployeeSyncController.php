<?php namespace App\Http\Controllers\Local;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Imports\MembersImport;
    use App\Imports\NonMembersImport;
    use App\Models\Employee;

    class EmployeeSyncController extends Controller
    {
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit()
        {
            return view('local.employees.sync');
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request)
        {
            Employee::truncate();

            \Excel::import(new MembersImport, $request->file('m_list'));
            \Excel::import(new NonMembersImport, $request->file('nm_list'));

            return redirect()->route('employees.index')
              ->withStatus('The employee data has been successfully synced!');
        }
    }
