<?php namespace App\Http\Controllers\Local;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Models\Unit;


    class UnitsController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $collection = Unit::paginate(25);

            return view('local.units.index')
              ->withUnits($collection);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('local.units.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
              $request->validate([
                  'name'                  => ['required'],
                  'address_line_one'      => ['required'],
                  'city'                  => ['required'],
                  'state'                 => ['required'],
                  'zip'                   => ['required'],
                  'finance_number'        => ['required']
              ]);

              Unit::create([
                'name'                    => $request->name,
                'address_line_one'        => $request->address_line_one,
                'address_line_two'        => $request->address_line_two,
                'city'                    => $request->city,
                'state'                   => $request->state,
                'zip'                     => $request->zip,
                'level'                   => $request->level,
                'finance_number'          => $request->finance_number,
                'phone_number'            => $request->phone_number,
                'fax_number'              => $request->fax_number
              ]);

              return redirect()->route('units.index')
                ->withStatus("The unit was created successfully!");
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(Unit $unit)
        {
            return view('local.units.edit')
              ->withUnit($unit);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Unit $unit)
        {
            $request->validate([
                'name'                  => ['required'],
                'address_line_one'      => ['required'],
                'city'                  => ['required'],
                'state'                 => ['required'],
                'zip'                   => ['required'],
                'finance_number'        => ['required']
            ]);

            $unit->update([
              'name'                    => $request->name,
              'address_line_one'        => $request->address_line_one,
              'address_line_two'        => $request->address_line_two,
              'city'                    => $request->city,
              'state'                   => $request->state,
              'zip'                     => $request->zip,
              'level'                   => $request->level,
              'finance_number'          => $request->finance_number,
              'phone_number'            => $request->phone_number,
              'fax_number'              => $request->fax_number
            ]);

            return redirect()->route('units.index')
              ->withStatus('The unit was updated successfully!');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(Unit $unit)
        {
            $unit->delete();

            return redirect()->route('units.index')
              ->withStatus('The unit has been deleted successfully!');
        }
    }
