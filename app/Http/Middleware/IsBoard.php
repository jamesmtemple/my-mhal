<?php namespace App\Http\Middleware;
    use Closure;

    class IsBoard
    {
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if(!auth()->check() || ! auth()->user()->is_board) {
              return redirect()->home();
            }

            return $next($request);
        }
    }
