<?php namespace App\Imports;
    use Illuminate\Support\Collection;
    use Maatwebsite\Excel\Concerns\ToModel;
    use App\Models\Employee;
    use Maatwebsite\Excel\Concerns\WithBatchInserts;
    use Maatwebsite\Excel\Concerns\WithHeadingRow;

    class NonMembersImport implements ToModel, WithBatchInserts, WithHeadingRow
    {
        public function model(array $row)
        {
            if($row['employeeid'] !==  null && $row['workfin_num'] !== null && $row['dcocraft'] >= 11) {
              return new Employee([
                  'first_name'        => $row['firstname'],
                  'middle_initial'    => $row['mi'],
                  'last_name'         => $row['lastname'],
                  'finance_number'    => $row['workfin_num'],
                  'pay_location'      => $row['payloc'],
                  'ein'               => $row['employeeid'],
                  'address_line_one'  => $row['addressline_1'],
                  'address_line_two'  => $row['addressline_2'],
                  'city'              => $row['city'],
                  'state'             => $row['state'],
                  'zip'               => $row['zip'],
                  'craft'             => ($row['dcocraft'] != 16) ? 37 : 38,
                  'status'            => ($row['dcocraft'] == 16) ? 11 : $row['dcocraft'],
                  'is_member'         => false
              ]);
            }
        }

        public function batchSize(): int
        {
            return 250;
        }
    }
