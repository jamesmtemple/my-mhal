<?php
    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Auth::routes();

    Route::get('/', 'HomeController@index')->name('home');

    Route::prefix('system')->namespace('System')->group(function(){
        Route::middleware('admin')->resource('users','UsersController');
    });

    Route::prefix('local')->namespace('Local')->group(function(){
        Route::middleware('admin')->resource('units','UnitsController');

        Route::middleware('admin')->get('employees/sync','EmployeeSyncController@edit')->name('employees.sync');
        Route::middleware('admin')->post('employees/sync','EmployeeSyncController@update')->name('employees.sync');
        Route::middleware('admin')->resource('employees','EmployeesController');

        Route::get('employees/find/{ein}', 'EmployeesController@show');

    });
