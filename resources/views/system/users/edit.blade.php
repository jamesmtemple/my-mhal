@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('users.create') }}" class="btn btn-secondary">Create New User</a>
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Edit User '{{ $user->profile->first_name }} {{ $user->profile->last_name }}'</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('users.update', $user) }}">
                                @csrf
                                @method('PATCH')

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('EIN') }}</label>

                                    <div class="col-md-6">
                                        <input id="ein" type="text" class="form-control{{ $errors->has('ein') ? ' is-invalid' : '' }}" name="ein" value="{{ old('ein', $user->ein) }}" required autofocus @blur="onBlur">

                                        @if ($errors->has('ein'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('ein') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="name" :value="firstName" disabled>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Middle Initial') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('middle_initial') ? ' is-invalid' : '' }}" name="middle_initial" :value="middleInitial" disabled>

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" :value="lastName" disabled>

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Finance Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="finance_number" type="text" class="form-control{{ $errors->has('finance_number') ? ' is-invalid' : '' }}" name="finance_number" :value="financeNumber" disabled>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('DCO Craft') }}</label>

                                    <div class="col-md-6">
                                        <input id="dco_craft" type="text" class="form-control{{ $errors->has('dco_craft') ? ' is-invalid' : '' }}" name="dco_craft" :value="dcoCraft" disabled>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                    <div class="col-md-6">
                                        <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username', $user->username) }}" required>

                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>

                                    <div class="col-md-6">
                                        <select class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="type" value="{{ old('type',$user->type) }}">
                                          <option value="0" @if(old('type',$user->type) === 0) selected @endif>Member</option>
                                          <option value="1" @if(old('type',$user->type) === 1) selected @endif>Steward</option>
                                          <option value="2" @if(old('type',$user->type) === 2) selected @endif>Director, Research and Education</option>
                                          <option value="3" @if(old('type',$user->type) === 3) selected @endif>Director, Human Relations</option>
                                          <option value="4" @if(old('type',$user->type) === 4) selected @endif>Director, Organization</option>
                                          <option value="5" @if(old('type',$user->type) === 5) selected @endif>Sergeant at Arms</option>
                                          <option value="6" @if(old('type',$user->type) === 6) selected @endif>Chief Steward</option>
                                          <option value="7" @if(old('type',$user->type) === 7) selected @endif>Director, Clerk Craft</option>
                                          <option value="8" @if(old('type',$user->type) === 8) selected @endif>Director, Maintenance Craft</option>
                                          <option value="9" @if(old('type',$user->type) === 9) selected @endif>Recording Secretary</option>
                                          <option value="10" @if(old('type',$user->type) === 10) selected @endif>Corresponding Secretary</option>
                                          <option value="11" @if(old('type',$user->type) === 11) selected @endif>Trustee</option>
                                          <option value="12" @if(old('type',$user->type) === 12) selected @endif>Treasurer</option>
                                          <option value="13" @if(old('type',$user->type) === 13) selected @endif>Vice President</option>
                                          <option value="14" @if(old('type',$user->type) === 14) selected @endif>President</option>
                                        </select>

                                        @if ($errors->has('type'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                @if(Auth::user()->type === 12)
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Authorized Payee') }}</label>

                                        <div class="col-md-6">
                                            <input type="checkbox" name="authorized_payee" @if($user->authorized_payee === true) checked @endif>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Authorized Signer') }}</label>

                                        <div class="col-md-6">
                                            <input type="checkbox" name="authorized_signer" @if($user->authorized_signer === true) checked @endif>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection


        @section('vue')
            <script type="text/javascript">
                new Vue({
                    el: "#app",

                    data: {
                      firstName: "{{ $user->profile->first_name }}",
                      middleInitial: "{{ $user->profile->middle_initial}}",
                      lastName: "{{ $user->profile->last_name }}",
                      financeNumber: "{{ $user->profile->finance_number }}",
                      dcoCraft: "{{ $user->profile->status }}"
                    },

                    methods: {
                      onBlur: function(event) {
                        axios.get("/local/employees/find/" + event.target.value)
                          .then((response) => {
                              data = response.data;

                              this.firstName = data.first_name;
                              this.middleInitial = data.middle_initial;
                              this.lastName = data.last_name;
                              this.financeNumber = data.finance_number;
                              this.daCode = data.status;
                          });
                      }
                    }
                });
            </script>
        @endsection
