@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('users.create') }}" class="btn btn-secondary">Create New User</a>
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">View User List</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="table-responsive">
                               <table class="table table-bordered">
                                  <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>EIN</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                  </tr>

                                  @foreach($users as $user)
                                    <tr>
                                      <td>{{ $user->profile->first_name }}</td>
                                      <td>{{ $user->profile->last_name }}</td>
                                      <td>{{ $user->ein }}</td>
                                      <td>{{ $user->type_text }}</td>
                                      <td>
                                        <a href="{{ route('users.edit', $user) }}" class="btn btn-secondary">Edit</a>

                                        <a class="btn btn-danger" href="{{ route('users.destroy', $user) }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('delete-{{ $user->id }}').submit();">Delete</a>
                                        <form id="delete-{{ $user->id }}" action="{{ route('users.destroy', $user) }}" method="POST">
                                          @method('DELETE')
                                          @csrf
                                        </form>
                                      </td>
                                    </tr>
                                  @endforeach

                               </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
