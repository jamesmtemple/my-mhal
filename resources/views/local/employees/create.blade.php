@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('employees.create') }}" class="btn btn-secondary mb-1">Create New Employee</a>
                          @if(Auth::user()->type === 12 OR Auth::user()->type === 14)<a href="{{ route('employees.sync') }}" class="btn btn-secondary">Sync Employee Data</a>@endif
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Create New Employee</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('employees.store') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Middle Initial') }}</label>

                                    <div class="col-md-6">
                                        <input id="middle_initial" type="text" class="form-control{{ $errors->has('middle_initial') ? ' is-invalid' : '' }}" name="middle_initial" value="{{ old('middle_initial') }}" autofocus>

                                        @if ($errors->has('middle_initial'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('middle_initial') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="ein" class="col-md-4 col-form-label text-md-right">{{ __('EIN') }}</label>

                                    <div class="col-md-6">
                                        <input id="ein" type="text" class="form-control{{ $errors->has('ein') ? ' is-invalid' : '' }}" name="ein" value="{{ old('ein') }}" required autofocus>

                                        @if ($errors->has('ein'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('ein') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="finance_number" class="col-md-4 col-form-label text-md-right">{{ __('Finance Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="finance_number" type="text" class="form-control{{ $errors->has('finance_number') ? ' is-invalid' : '' }}" name="finance_number" value="{{ old('finance_number') }}" required autofocus>

                                        @if ($errors->has('finance_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('finance_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Craft') }}</label>

                                    <div class="col-md-6">
                                        <select name="craft" class="form-control{{ $errors->has('craft') ? ' is-invalid' : ''}}">
                                          <option value="37" @if(old('craft','37') === 37) selected @endif>37 - Clerk</option>
                                          <option value="38" @if(old('craft','37') === 38) selected @endif>38 - Maintenance</option>
                                          <option value="39" @if(old('craft','37') === 39) selected @endif>39 - Motor Vehicle</option>
                                          <option value="38" @if(old('craft','37') === 40) selected @endif>40 - Operating Services</option>
                                          <option value="39" @if(old('craft','37') === 41) selected @endif>41 - Material Support</option>
                                        </select>
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                    <div class="col-md-6">
                                        <select name="status" class="form-control{{ $errors->has('craft') ? ' is-invalid' : ''}}">
                                          <option value="11" @if(old('craft','11') === 11) selected @endif>11 - FTR</option>
                                          <option value="21" @if(old('craft','11') === 21) selected @endif>21 - NTFT</option>
                                          <option value="31" @if(old('craft','11') === 31) selected @endif>31 - PTR</option>
                                          <option value="41" @if(old('craft','11') === 41) selected @endif>41 - PTF</option>
                                          <option value="81" @if(old('craft','11') === 81) selected @endif>81 - PSE</option>
                                        </select>
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="step" class="col-md-4 col-form-label text-md-right">{{ __('Level') }}</label>

                                    <div class="col-md-6">
                                        <select name="level" class="form-control{{ $errors->has('level') ? ' is-invalid' : ''}}" @change="onChange">
                                          <option value="3" @if(old('level','6') == 3) selected @endif>3</option>
                                          <option value="4" @if(old('level','6') == 4) selected @endif>4</option>
                                          <option value="5" @if(old('level','6') == 5) selected @endif>5</option>
                                          <option value="6" @if(old('level','6') == 6) selected @endif>6</option>
                                          <option value="7" @if(old('level','6') == 7) selected @endif>7</option>
                                          <option value="8" @if(old('level','6') == 8) selected @endif>8</option>
                                          <option value="9" @if(old('level','6') == 9) selected @endif>9</option>
                                          <option value="10" @if(old('level','6') == 10) selected @endif>10</option>
                                          <option value="11" @if(old('level','6') == 11) selected @endif>11</option>
                                        </select>
                                        @if ($errors->has('level'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('level') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="step" class="col-md-4 col-form-label text-md-right">{{ __('Step') }}</label>

                                    <div class="col-md-6">
                                        <select name="step" class="form-control{{ $errors->has('step') ? ' is-invalid' : ''}}">
                                          <option v-for="step in steps" :value="step" >@{{ step }}</option>
                                        </select>
                                        @if ($errors->has('step'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('step') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="address_line_one" class="col-md-4 col-form-label text-md-right">{{ __('Address Line One') }}</label>

                                    <div class="col-md-6">
                                        <input id="address_line_one" type="text" class="form-control{{ $errors->has('address_line_one') ? ' is-invalid' : '' }}" name="address_line_one" value="{{ old('address_line_one') }}" required autofocus>

                                        @if ($errors->has('address_line_one'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address_line_one') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address_line_two" class="col-md-4 col-form-label text-md-right">{{ __('Address Line Two') }}</label>

                                    <div class="col-md-6">
                                        <input id="address_line_two" type="text" class="form-control{{ $errors->has('address_line_two') ? ' is-invalid' : '' }}" name="address_line_two" value="{{ old('address_line_two') }}" autofocus>

                                        @if ($errors->has('address_line_two'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address_line_two') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required autofocus>

                                        @if ($errors->has('city'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                                    <div class="col-md-6">
                                        <select class="form-control {{ $errors->has('state') ? ' is-invalid' : ''}}" name="state" selected="NY">
                                          @foreach(CountryState::getStates('US') as $abbr => $state)
                                            <option value="{{ $abbr }}" @if(old('state', 'NY') === $abbr) selected @endif>{{ $state }}</option>
                                          @endforeach

                                        </select>

                                        @if ($errors->has('state'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ZIP') }}</label>

                                    <div class="col-md-6">
                                        <input id="zip" type="text" class="form-control{{ $errors->has('zip') ? ' is-invalid' : '' }}" name="zip" value="{{ old('zip') }}" required autofocus>

                                        @if ($errors->has('zip'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" autofocus>

                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Duty Hours') }}</label>

                                    <div class="col-md-6">
                                        <input id="duty_hours" type="text" class="form-control{{ $errors->has('duty_hours') ? ' is-invalid' : '' }}" name="duty_hours" value="{{ old('duty_hours') }}" autofocus>

                                        @if ($errors->has('duty_hours'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('duty_hours') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Off Days') }}</label>

                                    <div class="col-md-6">
                                        <input id="off_days" type="text" class="form-control{{ $errors->has('off_days') ? ' is-invalid' : '' }}" name="off_days" value="{{ old('off_days') }}" autofocus>

                                        @if ($errors->has('off_days'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('off_days') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Seniority Date') }}</label>

                                    <div class="col-md-6">
                                        <input id="seniority_date" type="date" class="form-control{{ $errors->has('seniority_date') ? ' is-invalid' : '' }}" name="seniority_date" value="{{ old('seniority_date') }}" autofocus>

                                        @if ($errors->has('seniority_date'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('seniority_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Preference Eligible') }}</label>

                                    <div class="col-md-6">
                                        <input type="checkbox" name="preference_eligible">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('APWU Member') }}</label>

                                    <div class="col-md-6">
                                        <input type="checkbox" name="is_member">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('vue')
        <script type="text/javascript">
            new Vue({
                el: "#app",

                data: {
                  steps: ["FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"],

                  level: "6"
                },

                methods: {
                  onChange: function(event) {
                    console.log(event.target.value);

                    switch(event.target.value){
                      case "3":
                      case "4":
                        this.steps = ["JJ","II","HH","GG","FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"];
                        break;
                      case "5":
                      case "6":
                      case "7":
                      case "8":
                          this.steps = ["FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"];
                          break;
                      case "9":
                      case "10":
                      case "11":
                          this.steps = ["D","E","F","G","H","I","J","K","L","M","N","O","P"];
                          break;
                    }
                  }
                }
            });
        </script>
    @endsection
