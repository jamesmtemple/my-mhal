@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('employees.create') }}" class="btn btn-secondary mb-1">Create New Employee</a>
                          @if(Auth::user()->type === 12 OR Auth::user()->type === 14)<a href="{{ route('employees.sync') }}" class="btn btn-secondary">Sync Employee Data</a>@endif
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Sync Employee Data</div>

                        <div class="card-body">
                            <p class="help-block">This feature allows authorized officers to sync the employee list using the APWU Member/Non-Member and DCO Reports provided by National. Please ensure you upload only the latest copy of each report, or you may cause inaccurate information to be synced to the MyMHAL Application.</p>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('employees.sync') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="m_list" class="col-md-4 col-form-label text-md-right">{{ __('Member List') }}</label>

                                    <div class="col-md-6">
                                        <input id="m_list" type="file" class="form-control-file{{ $errors->has('m_list') ? ' is-invalid' : '' }}" name="m_list" required autofocus>

                                        @if ($errors->has('m_list'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('m_list') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nm_list" class="col-md-4 col-form-label text-md-right">{{ __('Non-Member List') }}</label>

                                    <div class="col-md-6">
                                        <input id="nm_list" type="file" class="form-control-file{{ $errors->has('middle_initial') ? ' is-invalid' : '' }}" name="nm_list" required autofocus>

                                        @if ($errors->has('nm_list'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nm_list') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="dco_list" class="col-md-4 col-form-label text-md-right">{{ __('Dues Check Off') }}</label>

                                    <div class="col-md-6">
                                        <input id="dco_list" type="file" class="form-control-file{{ $errors->has('dco_list') ? ' is-invalid' : '' }}" name="dco_list" required autofocus>

                                        @if ($errors->has('dco_list'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('dco_list') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('vue')
        <script type="text/javascript">
            new Vue({
                el: "#app",

                data: {
                  steps: ["FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"],

                  level: "6"
                },

                methods: {
                  onChange: function(event) {
                    console.log(event.target.value);

                    switch(event.target.value){
                      case "3":
                      case "4":
                        this.steps = ["JJ","II","HH","GG","FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"];
                        break;
                      case "5":
                      case "6":
                      case "7":
                      case "8":
                          this.steps = ["FF","EE","DD","CC","BB","AA","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"];
                          break;
                      case "9":
                      case "10":
                      case "11":
                          this.steps = ["D","E","F","G","H","I","J","K","L","M","N","O","P"];
                          break;
                    }
                  }
                }
            });
        </script>
    @endsection
