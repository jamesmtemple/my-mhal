@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('employees.create') }}" class="btn btn-secondary mb-1">Create New Employee</a>
                          @if(Auth::user()->type === 12 OR Auth::user()->type === 14)<a href="{{ route('employees.sync') }}" class="btn btn-secondary">Sync Employee Data</a>@endif
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">View Employee List</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="table-responsive">
                               <table class="table table-bordered">
                                  <tr>
                                    <th>Name</th>
                                    <th>EIN</th>
                                    <th>Work Finance Number</th>
                                    <th>Actions</th>
                                  </tr>

                                  @foreach($employees as $employee)
                                    <tr>
                                      <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                                      <td>{{ $employee->ein }}</td>
                                      <td>{{ $employee->finance_number }}</td>
                                      <td>
                                        <a href="{{ route('employees.edit', $employee) }}" class="btn btn-secondary">Edit</a>

                                        <a class="btn btn-danger" href="{{ route('employees.destroy', $employee) }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('delete-{{ $employee->id }}').submit();">Delete</a>
                                        <form id="delete-{{ $employee->id }}" action="{{ route('employees.destroy', $employee) }}" method="POST">
                                          @method('DELETE')
                                          @csrf
                                        </form>
                                      </td>
                                    </tr>
                                  @endforeach

                               </table>
                            </div>

                            {{ $employees->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
