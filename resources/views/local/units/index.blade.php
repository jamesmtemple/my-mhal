@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('units.create') }}" class="btn btn-secondary">Create New Unit</a>
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">View Unit List</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="table-responsive">
                               <table class="table table-bordered">
                                  <tr>
                                    <th>Name</th>
                                    <th>Level</th>
                                    <th>Actions</th>
                                  </tr>

                                  @foreach($units as $unit)
                                    <tr>
                                      <td>{{ $unit->name }}</td>
                                      <td>{{ $unit->level }}</td>
                                      <td>
                                        <a href="{{ route('units.edit', $unit) }}" class="btn btn-secondary">Edit</a>

                                        <a class="btn btn-danger" href="{{ route('units.destroy', $unit) }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('delete-{{ $unit->id }}').submit();">Delete</a>
                                        <form id="delete-{{ $unit->id }}" action="{{ route('units.destroy', $unit) }}" method="POST">
                                          @method('DELETE')
                                          @csrf
                                        </form>
                                      </td>
                                    </tr>
                                  @endforeach

                               </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
