@extends('layouts.app')
    @section('content')
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card">
                      <div class="card-header">Essential Links</div>

                      <div class="card-body">
                          <a href="{{ route('units.create') }}" class="btn btn-secondary">Create New Unit</a>
                      </div>
                  </div>
              </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Edit Unit '{{ $unit->name }}'</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('units.update', $unit) }}">
                                @csrf
                                @method('PATCH')

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $unit->name) }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="level" class="col-md-4 col-form-label text-md-right">{{ __('Level') }}</label>

                                    <div class="col-md-6">
                                        <input id="level" type="text" class="form-control{{ $errors->has('level') ? ' is-invalid' : '' }}" name="level" value="{{ old('level', $unit->level) }}" required autofocus>

                                        @if ($errors->has('ein'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('ein') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="finance_number" class="col-md-4 col-form-label text-md-right">{{ __('Finance Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="finance_number" type="text" class="form-control{{ $errors->has('finance_number') ? ' is-invalid' : '' }}" name="finance_number" value="{{ old('finance_number', $unit->finance_number) }}" required autofocus>

                                        @if ($errors->has('finance_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('finance_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="address_line_one" class="col-md-4 col-form-label text-md-right">{{ __('Address Line One') }}</label>

                                    <div class="col-md-6">
                                        <input id="address_line_one" type="text" class="form-control{{ $errors->has('address_line_one') ? ' is-invalid' : '' }}" name="address_line_one" value="{{ old('address_line_one', $unit->address_line_one) }}" required autofocus>

                                        @if ($errors->has('address_line_one'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address_line_one') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address_line_two" class="col-md-4 col-form-label text-md-right">{{ __('Address Line Two') }}</label>

                                    <div class="col-md-6">
                                        <input id="address_line_two" type="text" class="form-control{{ $errors->has('address_line_two') ? ' is-invalid' : '' }}" name="address_line_two" value="{{ old('address_line_two', $unit->address_line_two) }}" autofocus>

                                        @if ($errors->has('address_line_two'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address_line_two') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $unit->city) }}" required autofocus>

                                        @if ($errors->has('city'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                                    <div class="col-md-6">
                                        <select class="form-control {{ $errors->has('state') ? ' is-invalid' : ''}}" name="state" selected="NY">
                                          @foreach(CountryState::getStates('US') as $abbr => $state)
                                            <option value="{{ $abbr }}" @if(old('state', $unit->state) === $abbr) selected @endif>{{ $state }}</option>
                                          @endforeach

                                        </select>

                                        @if ($errors->has('state'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ZIP') }}</label>

                                    <div class="col-md-6">
                                        <input id="zip" type="text" class="form-control{{ $errors->has('zip') ? ' is-invalid' : '' }}" name="zip" value="{{ old('zip', $unit->zip) }}" required autofocus>

                                        @if ($errors->has('zip'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number', $unit->phone_number) }}" autofocus>

                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Fax Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="fax_number" type="text" class="form-control{{ $errors->has('fax_number') ? ' is-invalid' : '' }}" name="fax_number" value="{{ old('fax_number', $unit->fax_number) }}" autofocus>

                                        @if ($errors->has('fax_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('fax_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
